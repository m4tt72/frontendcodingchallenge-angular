# FrontendCodingChallengeAngular

This angular projects lists the most starred Github repositories in 2017

## Instructions

Install node dependencies:

`npm install`

Run project

`ng serve`

## Libraries used

* [Bootstrap](https://getbootstrap.com/)
* [Moment.js](https://momentjs.com/)
* [ngx-infinite-scroll](https://www.npmjs.com/package/ngx-infinite-scroll)