import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { HomeRoutingModule } from './home-routing.module';
import { HomeComponent } from './home.component';
import { GithubService } from './services/github.service';
import { RepoComponent } from './components/repo/repo.component';
import { TimeAgoPipe } from './pipes/time-ago.pipe';
import { InfiniteScrollModule } from 'ngx-infinite-scroll';

@NgModule({
  declarations: [
    HomeComponent, 
    RepoComponent,
    TimeAgoPipe
  ],
  imports: [
    CommonModule,
    InfiniteScrollModule,
    HomeRoutingModule
  ],
  providers: [
    GithubService
  ]
})
export class HomeModule { }
