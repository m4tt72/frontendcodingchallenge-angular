import { Component, OnInit, Input } from '@angular/core';
import { Item } from '../../models/result.model';

@Component({
  selector: 'app-repo',
  templateUrl: './repo.component.html',
  styleUrls: ['./repo.component.scss']
})
export class RepoComponent implements OnInit {

  @Input()
  item: Item;
  
  constructor() { }

  ngOnInit() {
  }

}
