import { Component, OnInit, HostListener } from '@angular/core';
import { GithubService } from './services/github.service';
import { Item } from './models/result.model';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss']
})
export class HomeComponent implements OnInit {

  private page = 1;
  public items: Item[] = [];

  constructor(
    private githubService: GithubService
  ) { }

  async ngOnInit() {
    try {
      this.items = await this.githubService.getMostStarredRepos(this.page);
    } catch (error) {
      console.error(error);
    }
  }

  async onScroll() {
    try {
      this.page++;
      for (const item of await this.githubService.getMostStarredRepos(this.page)) {
        this.items.push(item);
      }
      console.log(this.items);
    } catch (error) {
      console.log(error);
    }
  }

}
