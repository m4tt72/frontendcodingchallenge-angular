import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Result, Item } from '../models/result.model';
import { environment } from 'src/environments/environment';
import { take, map } from 'rxjs/operators';

@Injectable()
export class GithubService {

  constructor(
    private http: HttpClient
  ) { }

  /**
   * Get the most starred repositories that were created in the last 30 days
   * Check environment.ts for the github API url.
   * @param page Page number, default to 1
   * @returns {Promise<Item[]>} list of repositories 
   */
  public getMostStarredRepos(page: number = 1): Promise<Item[]> {
    return this.http
      .get<Result>(`${environment.base_url}&page=${page}`)
      .pipe(
        take(1)
      )
      .pipe(
        map((result: Result) => result.items)
      )
      .toPromise()
  }

}
